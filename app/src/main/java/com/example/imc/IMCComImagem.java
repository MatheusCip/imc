package com.example.imc;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class IMCComImagem extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imc_com_imagem);
    }

    public void CalcularIMC (View view){
        EditText peso2 = findViewById(R.id.peso2);
        EditText altura2 = findViewById(R.id.altura2);
        ImageView resultadoImagem = findViewById(R.id.resultadoImagem);

        float pesoF = Float.parseFloat(peso2.getText().toString());

        float alturaF = Float.parseFloat(altura2.getText().toString());

        float IMC = pesoF / (alturaF * alturaF);

        if (IMC < 18.5){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(IMC) + " Abaixo do peso", Toast.LENGTH_LONG );
            Drawable drawable = getResources().getDrawable(R.drawable.magro);
            resultadoImagem.setImageDrawable(drawable);
        }

        if (IMC >= 18.6 && IMC < 25){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(IMC) + " Peso Ideal", Toast.LENGTH_LONG );
            Drawable drawable = getResources().getDrawable(R.drawable.magro2);
            resultadoImagem.setImageDrawable(drawable);
        }

        if (IMC >= 25 && IMC < 30){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(IMC) + " Acima do peso", Toast.LENGTH_LONG );
            Drawable drawable = getResources().getDrawable(R.drawable.gordo1);
            resultadoImagem.setImageDrawable(drawable);
        }

        if (IMC >= 30 && IMC < 35){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(IMC) + " Obesidade de grau 1", Toast.LENGTH_LONG );
            Drawable drawable = getResources().getDrawable(R.drawable.gordo2);
            resultadoImagem.setImageDrawable(drawable);
        }

        if (IMC >= 35){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(IMC) + " Obesidade de grau 2", Toast.LENGTH_LONG );
            Drawable drawable = getResources().getDrawable(R.drawable.gordo3);
            resultadoImagem.setImageDrawable(drawable);
        }
    }
}
