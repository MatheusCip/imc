package com.example.imc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class IMCSemImagem extends AppCompatActivity {

    EditText peso;
    EditText altura;
    TextView resultado;
    TextView resultado2;
    RadioButton masculino;
    RadioButton feminino;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imc_sem_imagem);

    }
    public void calcularIMC(View view) {


        peso = findViewById(R.id.peso);
        altura = findViewById(R.id.altura);
        resultado= findViewById(R.id.resultado);
        resultado2 = findViewById(R.id.resultado2);

        float pesoF = Float.parseFloat(peso.getText().toString());

        float alturaF = Float.parseFloat(altura.getText().toString());

        float IMC = pesoF / (alturaF * alturaF);

        String IMCFinal = String.valueOf(IMC);

        resultado.setText(IMCFinal);

        if(IMC < 18.5){
            resultado2.setText("Abaixo do peso!");
        }

        if(IMC > 18.6 && IMC < 24.5){
            resultado2.setText("Peso ideal (parabéns)!");
        }

        if (IMC > 25 && IMC < 29.9) {
            resultado2.setText("Levemente acima do peso!");
        }

        if (IMC > 30 && IMC < 34.5) {
            resultado2.setText("Obesidade grau I!");
        }

        if (IMC > 35 && IMC < 39.9) {
            resultado2.setText("Obesidade grau II (Severa)!");
        }

        if (IMC > 40) {
            resultado2.setText("Obesidade grau III (Mórbida)!");
        }
    }
}